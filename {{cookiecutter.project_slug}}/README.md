# Docker setup for {{ cookiecutter.project_slug }}

{% if cookiecutter.project_short_description %}
{{ cookiecutter.project_short_description }}
{% endif %}

This repository contains the files that are necessary to run and build the
images for the {{ cookiecutter.project_slug }} project.

## Installation

You need to have [docker](https://docs.docker.com/get-docker/) installed and
you need to clone this repository. We also recommend that you install
[`docker compose`](https://docs.docker.com/compose/install/).

Once you did this, create a folder named `{{ cookiecutter.project_slug }}` and
clone the `docker` repository:

```bash
# clone the repository
git clone https://{{ cookiecutter.gitlab_host }}/{{ cookiecutter.gitlab_username }}/{{ cookiecutter.project_slug }}/{{ cookiecutter.project_slug }}.git
# change into the cloned repository
cd {{ cookiecutter.project_slug }}
```
Now you need to build and deploy the images. We recommend that you do this via
`docker compose` (see below).

## Using docker compose

The recommended way to build and deploy the files in this repository is to
use `docker compose`. You can install this manually for your operating system
(see the [docker docs](https://docs.docker.com/compose/install/)).

### Building the images

You first need to build the images locally via

```bash
docker compose build
```

This will build two images, the `{{ cookiecutter.project_slug }}_django`
image and the `{{ cookiecutter.project_slug }}_nginx` image.

### Running the containers

To run the containers, simply do a

```bash
docker compose up
```

or if you want to detach it, run

```bash
docker compose up -d
```

### Deploying in production

If you want to deploy the containers in production, make sure to set the
secret variables in an `.env` file (see the [docs][env-file]). This file will
be ignored by the version control system. You need to add the following
variables in this `.env`-file:

- `POSTGRESQL_PASSWORD`: The password for the postgres user
- `ALLOWED_HOSTS`: This should include the host names that you allow from where
  the website is accessed (see [the django docs][ALLOWED_HOSTS]). Multiple hosts
  should be delimited by commata (e.g. `www.example.com,example.com`)
- `DJANGO_SECRET_KEY`: The secret key for the django application (see
  [the django docs][SECRET_KEY]). You can generate a random one via

  ```python
  from django.core.management.utils import get_random_secret_key
  print(get_random_secret_key())
  ```


[env-file]: (https://docs.docker.com/compose/env-file/)
[ALLOWED_HOSTS]: https://docs.djangoproject.com/en/3.2/ref/settings/#allowed-hosts
[SECRET_KEY]: https://docs.djangoproject.com/en/3.2/ref/settings/#secret-key


### Managing dependencies

The django-application is equipped with a Pipfile that is used by the docker
image during installation to install the dependencies. If you never worked
with a `Pipfile`, look into the [`pipenv`][pipenv] project.

#### Initial creation of `Pipfile.lock`

The docker image builds a `Pipfile.lock` from this file and it is recommended
to put this under version control, too. To do so,

1. build the image via `docker compose build django`
2. copy the `Pipfile.lock` to your code
3. start a new docker container and extract the contents of `Pipfile.lock` via
   `docker run --rm -it {{ cookiecutter.project_slug }}-django cat Pipfile.lock > docker/django/Pipfile.lock`
{%- if cookiecutter.use_reuse == "yes" %}
4. Add the license info via
   ```bash
   $ reuse annotate --year {{ cookiecutter.copyright_year }} --license {{ cookiecutter.supplementary_files_license }} --copyright "{{ cookiecutter.copyright_holder }}" --force-dot-license docker/django/Pipfile.lock
   ```
{% endif %}

#### Updating dependencies

Note that you should keep the `Pipfile.lock` up-to-date everytime you change
the requirements. Once you updated the `Pipfile`, you can either remove the
`Pipfile.lock` and rerun the steps in the *Initial creation of `Pipfile.lock`*,
or you update it in the container first by running

1. start a docker container via `docker run --rm -it --name {{ cookiecutter.project_slug }}-pipfile-generation {{ cookiecutter.project_slug }}-django /bin/bash`
2. use the `vi` editor to edit the `Pipfile` via `vi Pipfile`.
   If you never used `vi` before, here are some tips:

   1. hit `i` to go into insert mode
   2. navigate the cursor to the line you want to change
   3. when you are done with the changes, hit the `Esc`-button
   4. enter `:w` to save the file
   5. enver `:q` to quit the editor
3. Now you can generate a new lock file via `pipenv lock`. This will generate a
   new `Pipfile.lock` in the container
4. open a new terminal an copy the two files to your repositoy via

   ```bash
   docker cp {{ cookiecutter.project_slug }}-pipfile-generation:/opt/app-root/src/Pipfile docker/django/
   docker cp {{ cookiecutter.project_slug }}-pipfile-generation:/opt/app-root/src/Pipfile.lock docker/django/
   ```
5. Add and commit the two files

   ```bash
   git add docker/django/Pipfile docker/django/Pipfile.lock
   git commit -m "Updated dependencies"
   ```
6. Exit the container the you created in step 1 by entering `exit` into the
   first terminal

[pipenv]: https://pipenv.pypa.io/

## Building the images manually

If you do not want to use `docker compose`, you can also build the images
manually. Note that you than have to make sure to run a `postgres` and
`redis` container that are connected to the same network as the `django`
container. Here we only document how to build the `django` and `nginx` image.

### Building the python image

The source for the django-image lives in the
[django Dockerfile](docker/django/Dockerfile).
To build this docker image, run

```bash
docker build -t {{ cookiecutter.project_slug }}_django docker/django
```

### Building the nginx image

The django application is deployed through a dedicated container that is
running nginx and deploys the static files. This image copies the static files
from the django image (see above) in order to deploy them. Therefore you need
to provide the name of your image with the `djangoimage` build argument. If you
copied the above `django build` statement, this is
`{{ cookiecutter.project_slug }}_django`.

Once you build the python image as described above, you can build the `nginx`
image via

```bash
docker build --build-arg djangoimage={{ cookiecutter.project_slug }}_django -t {{ cookiecutter.project_slug }}_nginx -f docker/nginx/Dockerfile .
```

## Running tests

Different containers have different test setups that test the images. Checkout
the [docker-compose.test.yml](docker-compose.test.yml) file to
see the different test services. To run the nginx-tests for instance, run

```bash
docker compose -f docker-compose.yml -f docker-compose.test.yml \
  run --build nginx-test
```

after you built the *nginx*-image (see above).

### NGINX CIS Benchmarks

This repository contains the tests for the containers, in particular we test
the following CIS Benchmarks (see https://downloads.cisecurity.org/#/),
*CIS NGINX Benchmark, v2.0.0 - 11-01-2022*

If you want to exclude a specific benchmark in your tests, set the
`SKIP_NGINX_?_?_?` variable. `?_?_?` should represent the number of the
benchmark. If you want to skip an individual block, add a `# skip: CIS ?.?.?`
comment behind the line that you want to skip, e.g.

```
server_name _;  # skip: CIS 2.4.2
```

#### Coverage of Benchmarks

Our test suite does not cover all benchmarks. Most of the benchmarks are
trivial in our ubi/rhel-based container setup. You can find a detailed list
in the corresponding test file:
[`docker/nginx/tests/test_cis.bats`](docker/nginx/tests/test_cis.bats)

Certain benchmarks need to be configured and checked manually:

- *3.1 Ensure detailed logging is enabled (Manual)*

  The discussion about what should be logged and how is ongoing.
- *2.5.1 Ensure server_tokens directive is set to off*

  The *Audit* of this benchmark recommends to test against a running nginx
  but our test suite here only checks against the configuration.
- *5.1.2 Ensure only approved HTTP methods are allowed (Manual)*

  allowed methods are implemented in the config file at
  [`docker/nginx/conf/nginx-default-cfg/allowed_methods.conf`](docker/nginx/conf/nginx-default-cfg/allowed_methods.conf).
  A test in the sense of continuous integration requires testing against a
  running nginx that is not possible in our current setup.

#### Example
To run the tests but skip benchmark 2.4.1,
(*2.4.1 Ensure NGINX only listens for network connections on authorized ports*),
run

```bash
docker compose \
  -f docker-compose.yml -f tests/docker-compose.test.yml \
  run -e SKIP_NGINX_2_4_1=1 --build nginx-test
```


## About this repository

The skeleton for this repository has been generated from the
[cookiecutter][cookiecutter] template at

https://codebase.helmholtz.cloud/hcdc/software-templates/django-docker-template

[cookiecutter]: https://cookiecutter.readthedocs.io/en/stable/

### Authors:
{%- for author_info in get_author_infos(cookiecutter.project_authors.split(','), cookiecutter.project_author_emails.split(',')) %}
- [{{ author_info.full_name }}](mailto:{{ author_info.email }})
{% endfor %}


## Technical note

This package has been generated from the template
{{ cookiecutter._template }}.

See the template repository for instructions on how to update the skeleton for
this package.

### Maintainers
{%- for author_info in get_author_infos(cookiecutter.project_maintainers.split(','), cookiecutter.project_maintainer_emails.split(',')) %}
- [{{ author_info.full_name }}](mailto:{{ author_info.email }})
{% endfor %}

## License information

Copyright © {{ cookiecutter.copyright_year }} {{ cookiecutter.copyright_holder }}

{% if cookiecutter.use_reuse == "yes" %}
Code files in this repository is licensed under the {{ cookiecutter.code_license }}.

Documentation files in this repository is licensed under {{ cookiecutter.documentation_license }}.

Supplementary and configuration files in this repository are licensed
under {{ cookiecutter.supplementary_files_license }}.

Please check the header of the individual files for more detailed
information.

{% else %}
All rights reserved.
{% endif %}
