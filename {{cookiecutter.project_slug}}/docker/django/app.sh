# run scrip for the django application

python manage.py migrate --noinput

python manage.py register_host

daphne --verbosity 2 --access-log - -b 0.0.0.0 -p 8080 main.asgi:application
