"""{{ cookiecutter.project_slug }} URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
import datetime as dt
import socket
import textwrap

from cms.signals import urls_need_reloading


from django.contrib import admin
from django.conf import settings

from django.core import mail
from django.dispatch import receiver
from django.urls import include, path, re_path  # noqa: F401


urlpatterns = [
    path("accounts/", include("django.contrib.auth.urls")),
    {%- if cookiecutter.use_helmholtz_aai == "yes" %}
    path(
        "helmholtz-aai/",
        include("academic_community_helmholtz_aai.urls"),
    ),
    path("helmholtz-aai/", include("django_helmholtz_aai.urls")),
    {%- endif %}
    path(
        settings.ADMIN_LOCATION + "doc/",
        include("django.contrib.admindocs.urls"),
    ),
    path(settings.ADMIN_LOCATION, admin.site.urls),
    path("", include("academic_community.urls")),
]


@receiver(urls_need_reloading)
def mark_host_for_restart(**kwargs):
    from academic_community import utils

    ROOT_URL = getattr(settings, "ROOT_URL", "")

    mail.mail_admins(
        "%s portal requires restart" % utils.get_community_name(),
        textwrap.dedent(
            """
        Dear admins of the %s portal,

        an apphook has been modified and the server at %s requires a restart.

        Message info
        ------------
        Host: %s
        Time: %s

        Have a great day!
        """
            % (
                utils.get_community_name(),
                ROOT_URL + utils.get_index_location(),
                socket.gethostname(),
                dt.datetime.now().isoformat(),
            ),
        ),
    )
