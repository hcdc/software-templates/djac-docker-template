"""
Websocket routing configuration for the {{ cookiecutter.project_slug }} project.

It exposes the `websocket_urlpatterns` list, a list of url patterns to be used
for deployment.

See Also
--------
https://channels.readthedocs.io/en/stable/topics/routing.html
"""

from typing import List, Any

from django.urls import path  # noqa: F401

import academic_community.routing

websocket_urlpatterns: List[
    Any
] = academic_community.routing.websocket_urlpatterns + [
    # django project specific routes
]
